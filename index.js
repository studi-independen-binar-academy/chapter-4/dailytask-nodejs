const fs = require('fs');
const dataPerson = require('./person.json');

const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.render('personalData.ejs', {
        data: dataPerson
    });
});

app.listen(3000);