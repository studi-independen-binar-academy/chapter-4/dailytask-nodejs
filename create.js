const fs = require('fs');

const createPerson = function (person) {
    fs.writeFileSync('./person.json', JSON.stringify(person));
    return person;
}

const Tubagus = createPerson({
    name: 'Tubagus',
    age: 17,
    address: 'Konoha'
});